process.env.NODE_ENV = 'production'

const request = require("request");

let options = {
    method: 'DELETE',
    url: 'https://api.cloudflare.com/client/v4/zones/4f56c97f4c444b06e2bbb0148a66a8bc/purge_cache',
    headers:
        {
            'Cache-Control': 'no-cache',
            'Content-Type': 'application/json',
            'X-Auth-Key': process.env.CF_KEY,
            'X-Auth-Email': process.env.CF_EMAIL
        },
    body: {purge_everything: true},
    json: true
};

request(options, function (error, response, body) {
    if (error) throw new Error(error);

    console.log(body);
});

// ckp.io
options.url = 'https://api.cloudflare.com/client/v4/zones/9bd88826d9127acc82368fef6f09e903/purge_cache'

request(options, function (error, response, body) {
    if (error) throw new Error(error);

    console.log(body);
});
