// === DEFAULT / CUSTOM STYLE ===
// WARNING! always comment out ONE of the two require() calls below.
// 1. use next line to activate CUSTOM STYLE (./src/themes)
require(`./themes/app.${__THEME}.styl`)
// 2. or, use next line to activate DEFAULT QUASAR STYLE
// require(`quasar/dist/quasar.${__THEME}.css`)
// ==============================

// Uncomment the following lines if you need IE11/Edge support
// require(`quasar/dist/quasar.ie`)
// require(`quasar/dist/quasar.ie.${__THEME}.css`)
import Vue from 'vue'
import Quasar from 'quasar'
import router from './router'
import store from './store'

import {LocalStorage, Toast} from 'quasar'

import axios from 'axios'


import VueStash from 'vue-stash'

Vue.use(VueStash)

import mixinMain from 'mixins/mixinMain'

Vue.mixin(mixinMain)

import mixinLogin from 'mixins/mixinLogin'

Vue.mixin(mixinLogin)

import mixinFM from 'mixins/mixinFM'

Vue.mixin(mixinFM)

import mixinFMStorage from 'mixins/mixinFMStorage'

Vue.mixin(mixinFMStorage)

Vue.config.productionTip = false
Vue.use(Quasar) // Install Quasar Framework

if (__THEME === 'mat') {
    require('quasar-extras/roboto-font')
}
import 'quasar-extras/material-icons'
// import 'quasar-extras/ionicons'
import 'quasar-extras/fontawesome'
// import 'quasar-extras/animate'

axios.defaults.baseURL = process.env.API_PREFIX;
axios.defaults.headers.common['Authorization'] = 'Bearer ' + LocalStorage.get.item('token');

axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    let er = error.response

    window.ckp.$store.errors = er.data.errors
    window.ckp.$store.body = er.data

    if (er.status === 401) {
        Toast.create.negative({
            html: 'Please, login again',
            icon: 'lock',
            timeout: 2000,
        })

        router.push('/logout')
    }
    else if (er.status === 422) {
        Toast.create.info({
            html: er.data.message ? er.data.message : 'Please, check your data',
            icon: 'error',
        })
    }

    return Promise.reject(error);
});

window.ckp = new Vue({
    el: '#q-app',
    router,
    data: {store},
    render: h => h(require('./App').default),
})

Quasar.start(() => {
    window.ckp
})

Array.prototype.summer = function (item) {
    return this.reduce((a, b) => a + b[item], 0) || 0
}
