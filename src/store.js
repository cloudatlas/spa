export default {
    errors: {},
    body: {},
    token: null,
    afterLogin: '/fm',
    user: {},
    fm: {
        manager: {
            table: [],
            path: '',
            loading: false,
            clipboard: {},
        },
        storages: [],
        addStorageReset: {
            name: '',
            type: 'ftp',
            host: '',
            username: '',
            password: '',
            pkey: '',
            port: 21,
            root: '/',
            passive: true,
            ssl: false,
        }, addStorage: {
            name: '',
            type: 'ftp',
            host: '',
            username: '',
            password: '',
            pkey: '',
            port: 21,
            root: '/',
            passive: true,
            ssl: false,
        },
        savingStorage: false,
        editingStorage: '', // ID
        deletingStorage: [],
        uploads: {
            lastUid: null,
            groups: [],
            files: [],
            someSuccess: false,
            someFail: false,
            totalFiles: 0,
            totalSuccess: 0,
            totalFail: 0,
            totalFinished: 0,
            status: 0,
            active: false,
            progress: 0,

        }
    },
    globalRefs: {
        modals: {
            fmAddStorage: {}
        },
        tooltips: {
            fmAddStorage: {}
        },
    },
}
