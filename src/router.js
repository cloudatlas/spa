import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
import {LocalStorage, Toast} from 'quasar'

function load(component) {
    // '@' is aliased to src/components
    return () => System.import(`@/${component}.vue`)
}

const router = new VueRouter({
    /*
     * NOTE! VueRouter "history" mode DOESN'T works for Cordova builds,
     * it is only to be used only for websites.
     *
     * If you decide to go with "history" mode, please also open /config/index.js
     * and set "build.publicPath" to something other than an empty string.
     * Example: '/' instead of current ''
     *
     * If switching back to default "hash" mode, don't forget to set the
     * build publicPath back to '' so Cordova builds work again.
     */
    mode: 'history',
    routes: [
        {
            path: '/',
            component: load('index'),
            children: [
                {
                    path: 'fm',
                    component: load('file-manager/storage/list-storages'),
                    meta: {
                        title: 'Storage accounts',
                        icon: 'storage'
                    }
                },
                {
                    path: 'fm/:storage_id/:path(.*)?',
                    name: 'fm-list-files',
                    component: load('file-manager/file-manager'),
                    meta: {
                        title: 'File Manager',
                        shortTitle: '',
                        // subtitle: 'You can edit your code, delete, rename and move files between different clouds',
                        icon: 'storage'
                    }
                },
                {
                    path: 'ide/editor',
                    component: load('ide/editor'),
                    meta: {
                        title: 'Code Editor',
                        icon: 'code'
                    }
                }
            ]
        },
        {
            path: '/login',
            component: load('auth/login'),
            meta: {
                unguarded: true
            }
        },
        {
            path: '/logout',
            component: load('auth/logout')
        },
        {
            path: '/register',
            component: load('auth/login'),
            meta: {
                unguarded: true
            }
        }, ,
        {
            path: '/forgot',
            component: load('auth/login'),
            meta: {
                unguarded: true
            }
        },

        // Always leave this last one
        {path: '*', component: load('error404')} // Not found
    ]
})

router.beforeEach((to, from, next) => {
    if (typeof window.ckp === 'object') {
        window.ckp.$store.body = {}
        window.ckp.$store.errors = {}
    }
    if (to.meta.unguarded) { // Allowed without auth
        next()
    } else {
        if (!LocalStorage.has('token')) {
            if (!['/login', '/logout'].includes(to.path)) {
                LocalStorage.set('afterLogin', to.path) // Save to get back to the link tried
            }

            Toast.create({
                html: 'Please, login to start',
                icon: 'verified_user',
                timeout: 2000,
            })

            next({path: '/login'})
        } else {
            next()
        }
    }
})

export default router
