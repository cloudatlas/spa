import axios from 'axios'

import {Loading, Toast, SessionStorage} from 'quasar'

const mixinFM = {
    mounted() {
    },
    computed: {
        goUp() {
            if (!this.$route.params.path) return ''
            let old_path = this.$route.params.path.split('/')
            old_path.pop()

            const path = old_path.join('/')
            return `/fm/${this.$route.params.storage_id}/${path}`
        },
        icons_table() {
            let icons_table = {}
            const icons_by_ext = [
                ['fa-file-code-o', 'ahk,applescript,as,au3,bat,bas,cljs,cmd,coffee,duino,egg,egt,erb,hta,ibi,ici,ijs,ipynb,itcl,js,jsfl,lua,m,mrc,ncf,nuc,nud,nut,php,php3,pl,pm,ps1,ps1xml,psc1,psd1,psm1,py,pyc,pyo,r,rb,rdp,scpt,scptd,sdl,sh,syjs,sypy,tcl,vbs,xpl,ebuild'],
                ['fa-file-word-o', '0,1st,600,602,abw,acl,afp,ami,ans,asc,aww,ccf,csv,cwk,dbk,dita,docm,docx,dot,dotx,egt,epub,ezw,fdx,ftm,ftx,gdoc,hwp,hwpml,log,lwp,mbp,md,me,mcw,mobi,nb,nbp,neis,odm,odt,ott,omm,pages,pap,pdax,pdf,quox,rpt,sdw,se,stw,sxw,info,uof,uoml,via,wpd,wps,wpt,wrd,wrf,wri'],
                ['fa-file-excel-o', '123,ab2,ab3,aws,bcsv,clf,cell,csv,gsheet,numbers,gnumeric,ods,ots,qpw,sdc,slk,stc,sxc,tab,txt,vc,wk1,wk3,wk4,wks,wks,wq1,xlk,xls,xlsb,xlsm,xlsx,xlr,xlt,xltm,xlw'],
                ['fa-file-powerpoint-o', 'gslides,key,nb,nbp,odp,otp,pez,pot,pps,ppt,pptx,prz,sdd,shf,show,shw,slp,sspss,sti,sxi,thmx,watch'],
                ['fa-file-archive-o', '7z,ace,apk,android,arc,halo,ba,big,bld,cab,daa,windows,deb,dmg,promethean,jar,java,lbr,lbr,lqr,lha,pak,zip,rar,sen,sit,tar,tib,uha,zoo,zip,iso,umd,nrg,nero,sdi,mds,mdx,dmg,cdi,cue,cif,c2d,daa,b6t'],
                ['fa-file-audio-o', '3gp,aa,aac,aax,act,aiff,amr,ape,au,awb,dct,dss,flac,gsm,m4a,m4b,m4p,mp3,mpc,opus,raw,sln,tta,vox,wav,wma,wv,webm,8svx'],
                ['fa-file-video-o', 'webm,mkv,flv,flv,vob,ogv,drc,gif,gifv,mng,avi,mov,wmv,yuv,rm,rmvb,asf,amv,mp4,mpg,mpg,m4v,svi,3gp,3g2,mxf,roq,nsv'],
                ['fa-file-image-o', 'ase,art,blp,bmp,bti,cd5,cit,cpt,cr2,csp,cut,dds,dib,djvu,egt,exif,gpl,grf,icns,ico,iff,jpg,jpeg,jfif,jp2,jps,lbm,max,miff,mng,msp,nitf,otb,pbm,pc1,pc2,pc3,pcf,pcx,pgm,pi1,pi2,pi3,pct,png,pnm,pns,ppm,psb,psd,psp,px,pxm,pxr,qfx,raw,rle,sct,sgi,tga,tiff,tif,tiff,vtf,xbm,xcf,xpm,zif'],
                ['fa-file-pdf-o', 'pdf'],
                ['gif', 'gif'],
                ['fa-file-text-o', 'txt'],
                ['fa-file-code-o', 'php,html,htm,js,vue,env,rst,md'],
                ['fa-sliders', 'conf'],
            ]
            icons_by_ext.map(item => {
                let name = item[0]
                let exts = item[1].split(',')

                exts.map(ext => icons_table[ext] = name)
            })

            return icons_table
        },
    },
    methods: {
        successListing(r, path = null) {
            this.$store.fm.manager.loading = false

            if (r.data.ok === false) {
                Toast.create.warning('Operation unsuccessful')
            }

            if (path) {
                this.$store.fm.manager.path = path
            }
            else if (this.$route.params.path) {
                this.$store.fm.manager.path = this.$route.params.path
            }
            else {
                this.$store.fm.manager.path = '/'
            }
            if (r.data.list)
                this.$store.fm.manager.table = r.data.list

        },
        failListing() {
            this.$store.fm.manager.loading = false
            Loading.hide()

            Toast.create.warning('Operation failed')
        },
        listFiles(id, path = '', done = {}) {
            this.$store.fm.manager.loading = true

            axios.get(`fm/${id}?path=${path}`)
                .then(r => {
                    this.successListing(r, path);
                    done()
                })
                .catch(r => this.failListing)
        },
        deleteFiles(id, current_path, paths = {}) {
            this.$store.fm.manager.loading = true

            axios.post(`fm/delete/${id}`, {path: current_path, paths: paths})
                .then(r => this.successListing(r, current_path))
                .catch(r => this.failListing)
        },
        createDir(id, current_path, name) {
            this.$store.fm.manager.loading = true

            axios.get('fm/create', {params: {id: id, path: current_path, name: name}})
                .then(r => this.successListing(r, current_path))
                .catch(r => this.failListing)
        },
        createFile(id, current_path, name) {
            this.$store.fm.manager.loading = true

            axios.get('fm/create_file/' + id, {params: {path: current_path, name: name}})
                .then(r => this.successListing(r, current_path))
                .catch(r => this.failListing)
        },
        renameDoc(id, file_item, new_name) {
            this.$store.fm.manager.loading = true

            axios.get(`fm/${id}/edit`, {params: {file_item: file_item, new_name: new_name}})
                .then(r => this.successListing(r, this.$route.params.path))
                .catch(r => this.failListing)
        },
        toClipboard(clipboard) {
            if (typeof clipboard == 'object') {
                SessionStorage.set('clipboard', clipboard)
                this.$store.fm.manager.clipboard = clipboard
            }
        },
        fromClipboard(operation = 'copy', baseId, folder_path) {
            if (SessionStorage.has('clipboard')) {
                // try the copy/move
                if (!baseId) baseId = this.$route.params.storage_id
                if (!folder_path) folder_path = this.$route.params.path

                axios.post(`fm/${operation}`, {
                    from: SessionStorage.get.item('clipboard'),
                    to: {id: baseId, folder: folder_path}
                })
                    .then(r => this.successListing(r, folder_path))
                    .catch(r => this.failListing)

                // Clean clipboard regardless if was copied/moved
                SessionStorage.remove('clipboard')
                this.$store.fm.manager.clipboard = {}
            } else { // No clipboard
                Toast.create.warning('Nothing in clipboard')
            }

        },
        viewFile(id, file, done) {
            Loading.show()

            axios.post(`fm/read/${id}`, {file: file})
                .then(r => {
                    Loading.hide()
                    done(r.data)
                })
                .catch(r => {
                    Loading.hide()
                    this.failListing
                })
        },
        saveFile(id, path, contents, done = {}) {
            this.editor.saving = true
            axios.post(`fm/saveFileEdits/${id}`, {path, contents})
                .then(r => {
                    this.editor.savedCode = this.editor.code
                    this.editor.saving = false
                    done()
                })
                .catch(r => {
                    this.editor.saving = false
                    done()
                    this.failListing
                })
        },
    }
}

export default mixinFM
