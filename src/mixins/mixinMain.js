import axios from 'axios'

import {Loading, Toast, LocalStorage, QSpinnerGears} from 'quasar'


const mixinMain = {
    mounted() {

    },
    methods: {
        processResults(response) {
            let data, type, color = 'white', bgColor = 'primary';
            if (response.data) data = response.data
            if (data.message) {
                switch (data.message.type) {
                    case 'positive':
                        bgColor = '#42C461'
                        break
                    case 'negative':
                        bgColor = '#E04848'
                        break
                    case 'warning':
                        bgColor = '#F4B400'
                        break
                    case 'info':
                        bgColor = '#50D4EF'
                        break
                    default:
                        bgColor = '#288FE7'
                        break
                }
                Toast.create({
                    html: data.message.body,
                    icon: data.message.icon ? data.message.icon : 'info',
                    // timeout: 2500,
                    color: color,
                    bgColor: bgColor,
                    // button:  {
                    //   label: 'Undo',
                    //   handler() {
                    //     // Specify what to do when button is clicked/tapped
                    //   },
                    //   color: '#000'
                    // }
                })
            }

            if (data.storages) this.updateStorages(data.storages)
        },
        toastEncData() {
            Toast.create({
                html: 'Sensitive data is always encrypted',
                icon: 'vpn_lock',
                timeout: 2000
            })
        }

    }
}

export default mixinMain
