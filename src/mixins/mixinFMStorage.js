import axios from 'axios'

const mixinFMStorage = {
    mounted() {
    },
    methods: {
        listStorages(done) {
            axios.get('fm/storages')
                .then(response => this.updateStorages(response.data.storages))
        },
        updateStorages(up) {
            this.$store.fm.storages = up
        },
        dialogAddStorage() {
            this.$store.globalRefs.modals.fmAddStorage.open()
            this.$store.globalRefs.tooltips.fmAddStorage.close()
        },
        dialogAddStorageClosed() {
            this.$store.fm.editingStorage = '' // must be empty if not editing
            const reset = this.$store.fm.addStorageReset
            this.$store.fm.addStorage = reset
        },
        dialogEditStorage(id) {
            let storages = this.$store.fm.storages
            let item = storages.findIndex(el => el.id == id)

            if (item > -1) {
                this.$store.fm.editingStorage = id
                this.$store.fm.addStorage = storages[item]
                this.$store.globalRefs.modals.fmAddStorage.open()
            }
        },
        addStorage() {
            this.$store.fm.savingStorage = true;
            let
                request,
                data = this.$store.fm.addStorage,
                editing = this.$store.fm.editingStorage

            if (editing) request = axios.patch(`fm/storages/${editing}`, data)
            else request = axios.post('fm/storages', data)

            request.then(response => {
                this.processResults(response)
                this.$store.fm.savingStorage = false
                this.$store.globalRefs.modals.fmAddStorage.close()
            })
                .catch(error => {
                    this.$store.fm.savingStorage = false
                })
        },
        deleteStorage(id, done) {
            this.$store.fm.deletingStorage.push(id);
            axios.delete('fm/storages/' + id)
                .then(response => {
                    this.processResults(response)
                })
                .catch(error => {
                })
        },
        typeStorage(type) {
            switch (type) {
                case 's3':
                    return {icon: 'fa-amazon', desc: 'AWS S3', color: 'orange'}
                    break
                case 'sftp':
                    return {icon: 'fa-linux', desc: 'Secure FTP / SSH', color: 'dark'}
                    break
                case 'azure':
                    return {icon: 'fa-windows', desc: 'Microsoft Azure', color: 'blue'}
                    break
                case 'ftp':
                    return {icon: 'fa-sitemap', desc: 'FTP', color: 'brown'}
                    break
                default:
                    return {icon: 'fa-cloud', desc: 'other filesystem', color: 'brown'}
                    break
            }
        }
    }
}

export default mixinFMStorage
