import axios from 'axios'
import {Loading, Toast, LocalStorage, QSpinnerGears} from 'quasar'

const mixinLogin = {
    mounted() {
    },
    methods: {
        refreshToken() {
            axios.get('/token')
                .then(r => {
                    console.info(r)
                })
                .catch(e => {
                    console.warn(e)
                })
        },
        checkLogin() {
            if (this.getToken()) return true
            else return false
        },
        afterLogin() {
            if (LocalStorage.has('afterLogin')) {
                this.$router.push(LocalStorage.get.item('afterLogin'))
                LocalStorage.remove('afterLogin')
            } else {
                this.$router.push(this.$store.afterLogin)
            }
        },
        getToken() {
            if (this.$store && typeof this.$store.token === 'string') return this.$store.token
            else if (LocalStorage.has('token')) {
                let token = LocalStorage.get.item('token')
                this.$store.token = token
                return token
            }
            else return null
        },
        setToken(body) {
            if (typeof body.token === 'string') {
                LocalStorage.set('token', body.token)
                this.$store.token = body.token
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + body.token;

                // @TODO enable token refresh
            }
            if (typeof body.user === 'object') {
                LocalStorage.set('user', body.user)
                this.$store.user = body.user
            }
        },
    }
}

export default mixinLogin
